package Singleton;

public class Fabrique {

    private static Fabrique instance;

    private Fabrique() {

    }

    public static Fabrique getInstance() {
        if (null == instance) {
            instance = new Fabrique();
        }
        return instance;
    }
}
