package Interfaces;

public interface IFabrique {
    
    ILicorne creerLicorne();
    IEnvironnement creerEnv();
}
