
 //Factory Pattern -> Créer Licorne + environnement

public class Fabrique implements IFabrique {

    private static Fabrique instance;

    private Fabrique() {

    }

    public static Fabrique getFabrique() {
        if (instance == null) {
            instance = new Fabrique();
        }
        return instance;
    }

    public Licorne creerLicorne () {
        // return new Licorne();
        return new Licorne("Gisèle", "bleue");

    }

    public Environnement creerEnv () {
        //retourne new env
        return new Environnement();
    }

//Singleton Pattern

    public static Fabrique getInstance() {
        if (null == instance) {
            instance = new Fabrique();
        }
        return instance;
    }
}

