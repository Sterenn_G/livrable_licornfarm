package Factory;

class LicorneWithEnv {
    
    //Licorne;
    ILicorne licorne;
    IFabrique fabrique;

    LicorneWithEnv(IFabrique fabrique) {
        this.fabrique = fabrique;
    }

    public void montage() {

        licorne = fabrique.creerLicorne();
        licorne.setName("Candy");
        licorne.setCouleur("rose");
    }
}

