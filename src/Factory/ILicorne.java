package Factory;

public interface ILicorne {
    void setName(String name);
    void setCouleur(String couleur);
}