public class Fabrique implements IFabrique {

    private static Fabrique instance;

    private Fabrique() {

    }

    public static Fabrique getFabrique() {
        if (instance == null) {
            instance = new Fabrique();
        }
        return instance;
    }

    Licorne creerLicorne () {
        // return new Licorne();
        return new Licorne();

    }

}

