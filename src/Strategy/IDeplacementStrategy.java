package Strategy;

public interface IDeplacementStrategy {
    boolean deplace(int distance);
    
}